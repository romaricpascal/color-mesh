import { Vector3, Cylindrical } from 'three';
import { lch } from 'culori';

export function colorFromPoint(point) {
	const coordinates = new Cylindrical().setFromVector3(point);
	return {
		mode: 'lch',
		l: coordinates.y,
		c: coordinates.radius,
		h: (coordinates.theta * 180) / Math.PI,
	};
}

export function pointFromColor(color) {
	// Color may have been parsed from an hex, rgb or hsl string
	const { h, c, l } = lch(color);
	return new Vector3().setFromCylindrical(
		new Cylindrical(c, (h * Math.PI) / 180, l),
	);
}

import css from './css.js';
import scss from './scss.js';
import json from './json.js';

export default {
	css,
	scss,
	json,
};

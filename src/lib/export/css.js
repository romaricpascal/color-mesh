export default function toCss(
	colors,
	{ colorSetName = 'colors', selector = ':root' } = {},
) {
	return `${selector} {
${colors.map(toDeclaration(colorSetName)).join('\n')}
}`;
}

function toDeclaration(coloSetName) {
	return function (color, index) {
		return `  --${coloSetName}-${index}: ${color};`;
	};
}

export function withNumberPrecision(content, precision = 2) {
	return String(content).replace(/\d+(\.\d+)?/g, function (match) {
		return parseFloat(match)
			.toFixed(precision)
			.replace(/[.0]0+$/, '');
	});
}

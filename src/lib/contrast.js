import { wcagContrast } from 'culori';

const PRECISION = 0.01;

const RATIO_COMPARATORS = {
	3: over(PRECISION, { of: 3 }),
	4.5: over(PRECISION, { of: 4.5 }),
	7: over(PRECISION, { of: 7 }),
};

function comparatorFor(contrastRatio) {
	return (
		RATIO_COMPARATORS[contrastRatio] || over(PRECISION, { of: contrastRatio })
	);
}

export function findPositionsWithMinimunContrast({
	in: gradient,
	against,
	contrastRatios = [3, 4.5, 7],
} = {}) {
	// We could use custom stops spread regularly between 0 & 1
	// but that's require computing colors for each of them,
	// while we already have color stops
	const contrastGradient = gradient.stops.map(([color, position]) => [
		wcagContrast(color, against),
		position,
	]);

	// Prepare a hash for storing the intervals matching each ratios
	const intervalsForRatios = Object.fromEntries(
		contrastRatios.map((ratio) => [ratio, []]),
	);

	// An collect the intervals containing each of the selected ratios
	for (var i = 0; i < contrastGradient.length - 1; i++) {
		contrastRatios.forEach((contrastRatio) => {
			if (
				isContained(contrastRatio, {
					in: [contrastGradient[i][0], contrastGradient[i + 1][0]],
				})
			) {
				intervalsForRatios[contrastRatio].push([
					contrastGradient[i],
					contrastGradient[i + 1],
				]);
			}
		});
	}

	// Finally, bisect each interval looking for the closest ratio
	const result = mapValues(
		intervalsForRatios,
		(intervalsWithRatio, contrastRatio) => {
			const matches = comparatorFor(contrastRatio);
			return intervalsWithRatio
				.map((interval) =>
					bisectStops(interval, { gradient, against, contrastRatio, matches }),
				)
				.filter(Boolean)
				.map(([_, position]) => position);
		},
	);

	return result;
}

function bisectStops(
	range,
	{ gradient, against, contrastRatio, matches } = {},
) {
	// First, check if there's any need to bisect
	const startContrast = range[0][0];
	const endContrast = range[1][0];

	if (matches(startContrast)) {
		return range[0];
	}
	if (matches(endContrast)) {
		return range[1];
	}

	// Prevent infinite recursion
	if (Math.abs(startContrast - endContrast) < PRECISION) {
		return;
	}

	const startPosition = range[0][1];
	const endPosition = range[1][1];

	const midPosition = (startPosition + endPosition) / 2;
	const contrast = contrastAtPosition(midPosition, { in: gradient, against });

	if (isContained(contrastRatio, { in: [range[0][0], contrast] })) {
		return bisectStops([range[0], [contrast, midPosition]], {
			gradient,
			against,
			contrastRatio,
			matches,
		});
	} else {
		return bisectStops([[contrast, midPosition], range[1]], {
			gradient,
			against,
			contrastRatio,
			matches,
		});
	}
}

function mapValues(object, fn) {
	Object.keys(object).forEach((key) => {
		object[key] = fn(object[key], key, object);
	});
	return object;
}

function contrastAtPosition(position, { in: gradient, against }) {
	const color = gradient.interpolate(position);
	return wcagContrast(color, against);
}

function isContained(value, { in: [start, end] }) {
	return !sameSign(start - value, end - value);
}

function over(precision, { of: threshold }) {
	return function isOver(value) {
		return value > threshold && value - threshold < precision;
	};
}

function sameSign(a, b) {
	return a * b >= 0;
}

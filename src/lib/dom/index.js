/**
 * Selects the content of target `<input>` or `<textarea>` element.
 * The element is passed as a `target` "keyword param" so that the
 * function can be used as an event listener (focus, for ex.)
 * @param {HTMLInputElement|HTMLTextAreaElement} options.target
 */
export function selectContent({ target }) {
	target.select();
}

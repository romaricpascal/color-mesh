/**
 * Creates an event handler for keyboard events
 * that looks up the actual method to execute
 * in a hash of methods using the event's `code`:
 *
 * ```js
 * element.addEventListener(keydown, keyboardEventHandler({
 *   onArrowUp(event) {},
 *   onArrowDown(event) {},
 *   ...
 * }))
 * ```
 *
 * @param {Object} methods - The hash of methods
 * @param {Function} options.getMethodName - Customize how the method name is derived from event (default `on{keyboardEvent.code}`)
 * @param {Function} options.shouldPreventDefault - Customize if the handling the event should `preventDefault` (default `() => true`)
 * @returns {Function}
 */
export function keyboardEventHandler(
	methods,
	{
		getMethodName = (keyboardEvent) => `on${keyboardEvent.code}`,
		shouldPreventDefault = () => true,
	} = {},
) {
	return function (keyboardEvent) {
		const methodName = getMethodName(keyboardEvent);

		if (methods[methodName]) {
			if (shouldPreventDefault(keyboardEvent)) {
				keyboardEvent.preventDefault();
			}
			methods[methodName](keyboardEvent);
		}
	};
}

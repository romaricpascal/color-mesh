import { invlerp } from '../lib/math.js';
import { interpolate } from 'culori';

export function createGradient(colors) {
	const stopPositions = computeStopPositions(colors);
	const stops = colors.map((color, index) => [color, 1 - stopPositions[index]]);
	return {
		stops,
		interpolate: interpolate(stops),
	};
}

export function colorAtStop(gradient, index) {
	// TODO: Replace by polyfilled `?.at` call
	return gradient.stops[index] ? gradient.stops[index][0] : null;
}

export function firstColor(gradient) {
	return colorAtStop(gradient, 0);
}

export function lastColor(gradient) {
	return colorAtStop(gradient, gradient.stops.length - 1);
}

export function position(channelValue, gradientStops) {
	return invlerp(
		gradientStops[0][0].l,
		gradientStops[gradientStops.length - 1][0].l,
		channelValue,
	);
}

/**
 * Computes the positions of the gradient stops
 * of each color, based on its l channel value
 * respect to the min and max l channels;
 */
function computeStopPositions(colors) {
	const lChannels = colors.map((color) => color.l);
	const minL = Math.min(...lChannels);
	const maxL = Math.max(...lChannels);

	return colors.map((color) => invlerp(minL, maxL, color.l));
}

/**
 * Creates a function that forwards a DOM event
 * to the component's event
 * @param {Function} dispatch - Svelte's `dispatch` function, from `createEventDispatcher`
 * @returns {Function}
 */
export function createEventForwarder(dispatch) {
	return function forward(event) {
		// When re-forwarding an already forwarded event,
		// we need to make sure to forward the original event
		// not the Svelte event we're just receiving.
		// To distinguish them, we can rely on Svelte events
		// having a `null` `target`
		const originalEvent = event.target ? event : event.detail;
		dispatch(event.type, originalEvent);
	};
}

/**
 * Wraps an event handler expecting a DOM event
 * so it can be used to listen to forwarded events
 * from `createEventForwarder`
 * @param {Function} fn
 * @returns {Function}
 */
export function withDomEvent(fn) {
	return function (svelteEvent) {
		return fn(svelteToDom(svelteEvent));
	};
}

/**
 * Converts a Svelte event from `createEventForwarder`
 * back to its original DOM Event
 * @param {CustomEvent} svelteEvent
 * @returns
 */
export function svelteToDom(svelteEvent) {
	return svelteEvent.detail;
}

import { readable } from 'svelte/store';

/**
 * Creates a readable store to track changes in device pixel ratio
 * Based on https://developer.mozilla.org/en-US/docs/Web/API/Window/devicePixelRatio#monitoring_screen_resolution_or_zoom_level_changes
 */
export function pixelRatioStore() {
	return readable(window.devicePixelRatio, (set) => {
		let mediaQueryList;

		function updatePixelRatio() {
			set(window.devicePixelRatio);
			mediaQueryList = matchMedia(
				`(resolution: ${window.devicePixelRatio}dppx)`,
			);
			mediaQueryList.addEventListener('change', updatePixelRatio, {
				once: true,
			});
		}

		updatePixelRatio();

		return function stop() {
			mediaQueryList.removeEventListener('change', updatePixelRatio, {
				once: true,
			});
		};
	});
}

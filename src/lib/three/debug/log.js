export function onceLogger({ method = 'log' } = {}) {
	const loggedFor = new WeakSet();
	return function logOnceFor(object, ...logs) {
		if (!loggedFor.has(object)) {
			console[method](...logs);
			loggedFor.add(object);
		}
	};
}

export function timesLogger(times = 5, { method = 'log' } = {}) {
	let logCount = 0;
	return function log(...logs) {
		if (logCount < times) {
			console[method](...logs);
			logCount++;
		}
	};
}

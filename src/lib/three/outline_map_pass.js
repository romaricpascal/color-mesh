import { Pass } from 'three/examples/jsm/postprocessing/Pass.js';
import { WebGLRenderTarget, ShaderMaterial } from 'three';

const MATERIAL = new ShaderMaterial({
	uniforms: {
		uOutlineMapState: {
			value: null,
		},
	},
	vertexShader: `
    attribute float outlineMapState;

    varying float vOutlineMapState;

    void main()
    {
      vOutlineMapState = outlineMapState;
      gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    }
  `,
	fragmentShader: `

    varying float vOutlineMapState;

    void main()
    {
        gl_FragColor = vec4(vOutlineMapState, vOutlineMapState, vOutlineMapState, vOutlineMapState);
    }
  `,
	transparent: true,
});

/**
 * First pass for the rendering of outlines around selected dots.
 * Objects needing to be outlined declare an `outlineMapState` attribute
 * on their vertices, which is picked up by the shader to render a black (outlineMapStep = 0)
 * and white (outlineMapStep = 1) map which will be used for creating the outlines using dilation.
 * See outline_from_map_pass.js for more details about that step.
 */
export class OutlineMapPass extends Pass {
	constructor(scene, camera, renderTarget = new WebGLRenderTarget()) {
		super();
		// We're only looking to make a render at the right time
		// not to do anything with the video buffers
		this.needsSwap = false;

		this.scene = scene;
		this.camera = camera;

		this.renderTarget = renderTarget;

		this.originalMaterials = new WeakMap();
		this.originalVisibilities = new WeakMap();
	}

	setSize(width, height) {
		this.renderTarget.setSize(width, height);
	}

	render(renderer) {
		const currentRenderTarget = renderer.getRenderTarget();

		// Swap materials rather than use overrideMaterial
		// to account for the mesh possibly being hollow
		this.scene.traverse((object) => {
			if (object.userData.outlineMap == false) {
				this.originalVisibilities.set(object, object.visible);
				object.visible = false;
			}
			if (object.material) {
				this.originalMaterials.set(object, object.material);
				object.material = MATERIAL;
			}
		});
		renderer.setRenderTarget(this.renderTarget);
		renderer.render(this.scene, this.camera);

		this.scene.traverse((object) => {
			if (object.userData.outlineMap == false) {
				object.visible = this.originalVisibilities.get(object);
			}
			if (object.material) {
				object.material = this.originalMaterials.get(object);
			}
		});

		renderer.setRenderTarget(currentRenderTarget);
	}
}

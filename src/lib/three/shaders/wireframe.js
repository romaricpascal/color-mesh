import { Vector4, DoubleSide, BufferAttribute } from 'three';

// Alternative way to render wireframes, allowing for front/back culling
// Based on:
// https://github.com/mattdesl/webgl-wireframes/blob/gh-pages/lib/wire.frag
export const shader = {
	vertexShader: `
  attribute vec3 barycentric;

  varying vec3 vBarycentric;


  void main () {
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position.xyz, 1.0);
    vBarycentric = barycentric;
  }`,
	fragmentShader: `
  uniform float thickness;
  uniform vec4 color;

  varying vec3 vBarycentric;
  
  // Smoothstep for drawing clean lines (as far as I understand)
  float aastep (float threshold, float dist) {
    float afwidth = fwidth(dist) * 0.5;
    return smoothstep(threshold - afwidth, threshold + afwidth, dist);
  }
  
  void main () {
    // this will be our signed distance for the wireframe edge
    float d = min(min(vBarycentric.x, vBarycentric.y), vBarycentric.z);
  
    // compute the anti-aliased stroke edge  
    float edge = 1.0 - aastep(thickness, d);
        
    gl_FragColor = vec4(color.xyz, edge);
  }
  `,
	transparent: true,
	alphaToCoverage: true,
	side: DoubleSide,
	extensions: {
		derivatives: true,
	},
};

export function createUniforms({
	thickness,
	color = new Vector4(1.0, 1.0, 1.0, 1.0),
} = {}) {
	return {
		thickness: { value: thickness },
		color: { value: color },
	};
}

/**
 * @param {BufferGeometry} bufferGeometry - A *non-indexed* BufferGeometry on which to add the barycentric coordinates
 */
export function addBarycentricCoordinates(bufferGeometry) {
	const attrib =
		bufferGeometry.getIndex() || bufferGeometry.getAttribute('position');
	const count = attrib.count / 3;
	const barycentric = [];

	// for each triangle in the geometry, add the barycentric coordinates
	for (let i = 0; i < count; i++) {
		const even = i % 2 === 0;
		if (even) {
			barycentric.push(0, 0, 1, 0, 1, 0, 1, 0, 0);
		} else {
			barycentric.push(0, 1, 0, 0, 0, 1, 1, 0, 0);
		}
	}

	// add the attribute to the geometry
	const array = new Float32Array(barycentric);
	const attribute = new BufferAttribute(array, 3);
	bufferGeometry.setAttribute('barycentric', attribute);
}

export function setupAttributes(geometry) {
	addBarycentricCoordinates(geometry);
}

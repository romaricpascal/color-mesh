import { Float32BufferAttribute } from 'three';

export function setAttribute(
	geometry,
	attributeName,
	value, // TODO: Handle array values && values for different indices
	{ BufferAttributeType = Float32BufferAttribute } = {},
) {
	const values = [];
	for (let i = 0; i < geometry.attributes.position.count; i++) {
		values.push(value);
	}
	geometry.setAttribute(attributeName, new BufferAttributeType(values, 1));
}

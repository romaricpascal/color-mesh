import { Triangle, Vector3 } from 'three';

/**
 * Finds the closest point on the geometry by iterating over its `faces`
 * @param {BufferGeometry} geometry - Needs a `.faces` method returning faces (objects with `a`,`b`,`c` indices of the point's `positions`)
 * @param {Vector3} point
 * @param {Object} [options]
 * @param {Function<Boolean>} [options.ignoreFace] - A function called before doing any heavy computation with the face
 * @param {Function<Vector3|undefined>} [options.closestFacePointToPoint] - A function computing the `target` point on the given `triangle` that's closest to the given `point`.
 * @returns
 */
export function closestPointToPoint(
	geometry,
	point,
	{
		ignoreFace = () => false,
		closestFacePointToPoint = closestTrianglePointToPoint,
	} = {},
) {
	const faces = geometry.faces;
	const positions = geometry.getAttribute('position');

	const a = new Vector3();
	const b = new Vector3();
	const c = new Vector3();
	const triangle = new Triangle(a, b, c);
	const closestPointOnFace = new Vector3();

	let minDistance = Infinity;
	let closestPointOnGeometry = new Vector3();
	let closestFace = new Triangle();
	let normalOfClosestFace = new Vector3();

	for (var i = 0; i < faces.length; i++) {
		triangle.setFromAttributeAndIndices(
			positions,
			faces[i].a,
			faces[i].b,
			faces[i].c,
		);

		if (!ignoreFace(triangle, point, i)) {
			const intersection = closestFacePointToPoint(
				triangle,
				point,
				closestPointOnFace,
			);

			if (intersection) {
				const distance = intersection.distanceTo(point);
				if (distance < minDistance) {
					minDistance = distance;
					closestPointOnGeometry.copy(intersection);
					closestFace = {
						...faces[i],
						normal: Triangle.getNormal(
							triangle.a,
							triangle.b,
							triangle.c,
							normalOfClosestFace,
						),
					};
				}
			}
		}
	}

	return { point: closestPointOnGeometry, face: closestFace };
}

function closestTrianglePointToPoint(triangle, point, target) {
	return triangle.closestPointToPoint(point, target);
}

/**
 * The modulo (`%`) operator returns negative values
 * if the dividend is negative. It usually doesn't impact
 * maths, but human will prefer to read a positive value
 * @param {Number} dividend
 * @param {Number} divisor - A non-zero dividend
 */

export function positiveModulo(dividend, divisor) {
	return (divisor + (dividend % divisor)) % divisor;
}

/**
 * Linear interpolation of `t` between `origin` and `target`
 * @param {Number} origin
 * @param {Number} target
 * @param {Number} t
 * @returns {Number}
 */
export function lerp(origin, target, t) {
	return origin + t * (target - origin);
}

/**
 * Inverse linear interpolation of `value` between `origin` and `target`
 * @param {Number} origin
 * @param {Number} target
 * @param {Number} t
 * @returns {Number}
 */
export function invlerp(origin, target, value) {
	return (value - origin) / (target - origin);
}

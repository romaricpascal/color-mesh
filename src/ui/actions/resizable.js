import { dispatch } from '../../lib/dom/events/dispatch.js';

const OBSERVER = new ResizeObserver((entries) => {
	for (let entry of entries) {
		const contentBoxSize = Array.isArray(entry.contentBoxSize)
			? entry.contentBoxSize[0]
			: entry.contentBoxSize;

		dispatch(entry.target, 'element-resized', {
			bubbles: true,
			detail: { contentBoxSize, contentRect: entry.contentRect },
		});
	}
});

export function resizable(element) {
	OBSERVER.observe(element);

	return function () {
		OBSERVER.unobserve(element);
	};
}

import { dispatch } from '../../lib/dom/events/dispatch.js';

// Based on Svelte's tutorial pannable action
export function pannable(node) {
	let x;
	let y;
	let clearListeners;

	function handleStart(event) {
		// Prevent another pan starting
		if (!clearListeners) {
			event.preventDefault();
			const positionSource = getPositionSource(event);
			x = positionSource.clientX;
			y = positionSource.clientY;

			dispatch(node, 'panstart', {
				detail: {
					x,
					y,
					event,
				},
			});

			if (event.type == 'mousedown') {
				clearListeners = addMouseListeners();
			} else {
				clearListeners = addTouchListeners();
			}
		}
	}

	function handleMove(event) {
		const positionSource = getPositionSource(event);
		const dx = positionSource.clientX - x;
		const dy = positionSource.clientY - y;
		x = positionSource.clientX;
		y = positionSource.clientY;

		dispatch(node, 'panmove', {
			detail: {
				x,
				y,
				dx,
				dy,
				event,
			},
		});
	}

	function handleEnd(event) {
		const positionSource = getPositionSource(event);
		if (positionSource) {
			x = positionSource.clientX;
			y = positionSource.clientY;

			dispatch(node, 'panend', {
				detail: {
					x,
					y,
					event,
				},
			});
		}

		// Shouldn't happen, but better safe than sorry
		if (clearListeners) {
			clearListeners();
		}
		clearListeners = null;
	}

	node.addEventListener('mousedown', handleStart);
	node.addEventListener('touchstart', handleStart);

	return {
		destroy() {
			node.removeEventListener('mousedown', handleStart);
			node.removeEventListener('touchstart', handleStart);
			// In case the action gets destroyed while dragging
			if (clearListeners) {
				clearListeners();
			}
		},
	};

	function addMouseListeners() {
		window.addEventListener('mousemove', handleMove);
		window.addEventListener('mouseup', handleEnd);

		return function () {
			window.removeEventListener('mousemove', handleMove);
			window.removeEventListener('mouseup', handleEnd);
		};
	}

	function addTouchListeners() {
		window.addEventListener('touchmove', handleMove);
		window.addEventListener('touchend', handleEnd);
		window.addEventListener('touchcancel', handleEnd);

		return function () {
			window.removeEventListener('touchmove', handleMove);
			window.removeEventListener('touchend', handleEnd);
			window.removeEventListener('touchcancel', handleEnd);
		};
	}
}

function getPositionSource(event) {
	if (window.TouchEvent && event instanceof TouchEvent) {
		return event.targetTouches[0];
	} else {
		return event;
	}
}

import { Triangle, Plane, Vector3, Line3 } from 'three';

import {
	indicesOfEdgeBetweenSets,
	indicesOfMeridians,
} from './color_system_geometry.js';

/**
 * Computes which points of the given ColorSystemGeometry
 * intersect with the given `slicePlane`, only considering
 * those in front of the `cullingPlane`
 * https://stackoverflow.com/questions/42348495/three-js-find-all-points-where-a-mesh-intersects-a-plane
 * @param {ColorSystemGeometry} colorSystemGeometry
 * @param {Plane} slicePlane
 * @param {Plane} cullingPlane
 */
export function slice(colorSystemGeometry, { point }) {
	// The slicing plane will be the one containing
	// both the Y axis and the intersection point
	const slicePlane = new Plane();
	slicePlane.setFromCoplanarPoints(
		new Vector3(),
		new Vector3(0, 1, 0),
		new Vector3(point.x, point.y, point.z),
	);

	// But we only want to consider intersection
	// with edges on the same side as the intersection point
	const cullingPlane = new Plane();
	cullingPlane.setFromNormalAndCoplanarPoint(
		new Vector3(point.x, 0, point.z),
		new Vector3(),
	);
	cullingPlane.normalize();

	// Iterate over "horizontal" edges
	const positions = colorSystemGeometry.getAttribute('position');

	const edgesBetweenSets = indicesOfEdgeBetweenSets(
		colorSystemGeometry.numberOfSets,
		colorSystemGeometry.colorsPerSet,
	).map(([startIndex, endIndex]) => {
		return new Line3(
			vector3At(positions, startIndex),
			vector3At(positions, endIndex),
		);
	});

	// (Opt) filter out the "back" edges
	// Get intersections
	return edgesBetweenSets
		.filter(inFrontOf(cullingPlane))
		.map((line) => {
			return slicePlane.intersectLine(line, new Vector3());
		})
		.filter(Boolean);
}

/**
 * Creates a function that checks if the given line
 * has any point in front of the given culling plane.
 * @param {Plane} cullingPlane
 * @returns Function
 */
function inFrontOf(cullingPlane) {
	return function (line) {
		return (
			// Positive dot product means we're in front
			// as the plane contains the Y axis. This'd need
			// reworking to work with arbitrary planes
			// https://en.wikipedia.org/wiki/Dot_product#Scalar_projection_and_first_properties
			line.start.dot(cullingPlane.normal) >= 0 ||
			line.end.dot(cullingPlane.normal) >= 0
		);
	};
}

const BARY_COORDINATES_B = new Vector3(0, 1, 0);
const BARY_COORDINATES_BC = new Vector3(0, 1, 1);

export function contour(colorSystemGeometry, { point, face }) {
	const positions = colorSystemGeometry.getAttribute('position');

	const triangle = new Triangle().setFromAttributeAndIndices(
		positions,
		face.a,
		face.b,
		face.c,
	);

	const barycentricCoordinates = triangle.getBarycoord(point, new Vector3());

	// Thanks to the way the geometry is constructed
	// the edge bridging two color sets is always BC
	// Now we want to get a point on that edge, following
	// the side of the triangle making the geometry's "meridians",
	// which is AB. To project on the edge, we need to make "x" 0
	// IDEA: It may be more accurate to project using UV coordinates
	// averaging the "meridian" sides of the two consecutive triangles
	const onEdge = barycentricCoordinates.add(
		new Vector3(-1, 1, 0).multiplyScalar(barycentricCoordinates.x),
	);

	const ratio =
		new Vector3().subVectors(onEdge, BARY_COORDINATES_B).length() /
		BARY_COORDINATES_BC.length();

	const startColorSet = Math.floor(face.b / colorSystemGeometry.colorsPerSet);
	// It's tempting to go with `startColorSet + 1`,
	// but we need to use the second edge of the line
	// so we handle properly the edges linking the first and last
	// color set
	const endColorSet = Math.floor(face.c / colorSystemGeometry.colorsPerSet);

	const meridians = indicesOfMeridians(
		startColorSet,
		endColorSet,
		colorSystemGeometry.colorsPerSet,
		colorSystemGeometry.numberOfSets,
	);

	return meridians.map(([startIndex, endIndex]) => {
		const meridianStart = vector3At(positions, startIndex);
		const meridianEnd = vector3At(positions, endIndex);

		return new Vector3()
			.subVectors(meridianEnd, meridianStart)
			.multiplyScalar(ratio)
			.add(meridianStart);
	});

	// Iterate over the edges bridging the two sets
	// to compute the intersection proportionally
}

function vector3At(bufferAttribute, index) {
	return new Vector3(
		bufferAttribute.getX(index),
		bufferAttribute.getY(index),
		bufferAttribute.getZ(index),
	);
}

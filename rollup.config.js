import { visualizer } from 'rollup-plugin-visualizer';

export default {
	plugins: [visualizer()],
	output: {
		manualChunks: {
			// Not quite sure how it works, but by asking Rollup
			// to set ThreeJS dependencies in a separate chunk,
			// it ends up bundling those that don't overlap either
			// in the `index` chunk or the `Visualization` chunk
			// which is exactly the intended effect!
			three: ['three'],
		},
	},
};

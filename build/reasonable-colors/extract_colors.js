#! /usr/bin/env node
/**
 * Extract colors from the CSS exported by the `export_colors.scss` file
 * Usage:
 *
 * ```sh
  build/reasonable-colors/extract_colors.js < node_modules/reasonable-colors/reasonable-colors.css > src/data/reasonable-colors.js
 * ```
 */

const EXTRACTION_PATTERN = /\s*?--color-(\w*?)-(\d{1}): (#.{6});/g;

process.stdin.on('data', (data) => {
	const result = {};
	data
		.toString()
		.replace(EXTRACTION_PATTERN, (_, colorSetName, colorName, color) => {
			if (!result[colorSetName]) {
				result[colorSetName] = {};
			}

			result[colorSetName][colorName] = color;
		});
	console.log(`
    // https://github.com/matthewhowell/reasonable-colors
    export default ${JSON.stringify(result, null, 2)}
  `);
});

#! /usr/bin/env node
/**
 * Extract colors from the CSS exported by the `export_colors.scss` file
 * Usage:
 *
 * ```sh
 * npx sass build/bootstrap/export_colors.scss | node build/bootstrap/extract_colors.js > src/data/bootstrap.js
 * ```
 */

const EXTRACTION_PATTERN = /\s*?(\w*?)-(\d{3}): (#.{6});/g;

process.stdin.on('data', (data) => {
	const result = {};
	data
		.toString()
		.replace(EXTRACTION_PATTERN, (_, colorSetName, colorName, color) => {
			if (!result[colorSetName]) {
				result[colorSetName] = {};
			}

			result[colorSetName][colorName] = color;
		});
	console.log(`
    // https://github.com/twbs/bootstrap
    export default ${JSON.stringify(result, null, 2)}
  `);
});

module.exports = {
  '*.js': ['eslint --fix', 'prettier --write'],
  '*.svelte': ['eslint --fix', 'stylelint --fix', 'prettier --write'],
  '*.{css,scss}': ['stylelint --fix', 'prettier --write'],
};

module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
    es6: true,
  },
  plugins: ['svelte3'],
  rules: {
    // Because the .prettierrc file is `.cjs`
    'prettier/prettier': ['error', require('./.prettierrc.cjs')],
    // Force extensions in imports to ensure library work OK when `import`ed in browser
    'import/extensions': ['error', 'ignorePackages'],
    'no-shadow': [
      'error',
      {
        builtinGlobals: true,
        hoist: 'all',
        allow: ['name', 'event', 'stop', 'origin'],
      },
    ],
    // Import from 'three' directly to avoid bundling modules twice
    // due to post-processing modules doing `import ... from 'three'`
    'no-restricted-imports': [
      'error',
      {
        patterns: ['three/src/*'],
      },
    ],
    'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
  },
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3',
      rules: {
        'prettier/prettier': 0,
      },
    },
    {
      files: ['*.test.js'],
      extends: ['plugin:mocha/recommended'],
      parserOptions: {
        // Re-set ecmaVersion that's being unset somehow
        ecmaVersion: 2020,
      },
      env: {
        browser: false,
      },
      globals: {
        expect: 'readonly',
      },
    },
    {
      files: ['cypress/**/*.js', '**/*.spec.js'],
      env: {
        browser: false,
      },
      extends: ['plugin:cypress/recommended'],
    },
  ],
};
